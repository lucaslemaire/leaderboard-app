# Leaderboard app
## Requirements
- node & npm (only tested with latest versions, node v20 & npm v10)

## Usage
- Clone this repository
- `cd leaderboard-app`
- `npm install`
- `npm start` or `npm test`

The available API requests are described in leaderboard.http

## Next Steps
For production readiness:
- Logs & metrics, for monitoring and alerting
- A database engine that scales to replace sqlite
- CI/CD to deploy this application on production servers, so that we can fix quickly if there is an issue

Depending on the usage:
- Api security (api key, oauth...) if the service is exposed on public networks, and/or privileges support if the people retrieving the leaderboard are not the same as the ones who update it
- Api documentation (swagger/openapi/postman collection...). If this service is only meant to be used by another nodejs service, the documentation could be replaced by a npm module wrapping the http requests & responses written in this repository
- Containerization for the app and database to ensure environments compatibility
- For high throughput, implement PlayerProvider with a memory cache (using a centralized cache service like redis if the application is running on multiple servers)
- For high throughput with resources cost reduction, horizontal auto-scaling orchestration of this service and load balancing (we would have to add api health checks)
- If the tournaments have a lot of players, pagination support over the leaderboard retrieval
