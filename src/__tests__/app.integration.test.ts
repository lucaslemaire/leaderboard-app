import * as fs from "fs";
import request from 'supertest';
import { Server } from 'http';
import { Database } from "sqlite3";
import App from '../app';
import config from '../config';
import ConnectDatabase from "../leaderboard/persistence";
import { Player } from "../leaderboard/models";

describe('Leaderboard web api', () => {
  let server: Server;
  let database: Database;
  const dbPath = `${__dirname}/player-test.db`;
  const app = App();

  beforeEach(async () => {
    if (fs.existsSync(dbPath)) {
      fs.unlinkSync(dbPath);
    }
    config.db.path = dbPath;
    database = await ConnectDatabase(config.db);
    await initDatabase();
    server = await app.start(config);
  });

  afterEach(async () => {
    await app.stop();
    database.close();
  });

  it('creates a player', async() => {
    const playerRequest = {
      username: 'testplayer'
    };

    const { body: resultBody } = await request(server)
      .post('/v1/leaderboard/player')
      .set('Content-Type', 'application/json')
      .send(playerRequest)
      .expect(201);

    const expectedPlayer = { username: playerRequest.username, score: 0 };
    expect(resultBody).toEqual(expectedPlayer);
    const savedPlayers = await getPlayersFromDatabase();
    expect(savedPlayers.length).toEqual(1);
    expect(savedPlayers[0]).toEqual(expectedPlayer);
  });

  it('cannot create a player without username', async() => {
    await request(server)
      .post('/v1/leaderboard/player')
      .set('Content-Type', 'application/json')
      .send({})
      .expect(400);
  });

  it('cannot create a player with an already existing username', async() => {
    const username = 'testplayer';
    await createPlayerInDatabase({ username, score: 0 });
    await request(server)
      .post('/v1/leaderboard/player')
      .set('Content-Type', 'application/json')
      .send({ username })
      .expect(409);
  });

  it('retrieves players', async() => {
    const playerA = { username: 'aaa', score: 0 };
    const playerB = { username: 'bbb', score: 10 };
    await createPlayerInDatabase(playerA);
    await createPlayerInDatabase(playerB);

    const { body: retrievedPlayerA } = await request(server)
      .get(`/v1/leaderboard/player/${playerA.username}`)
      .expect(200);
    const { body: retrievedPlayerB } = await request(server)
      .get(`/v1/leaderboard/player/${playerB.username}`)
      .expect(200);

    expect(retrievedPlayerA).toEqual({ ...playerA, position: 2 });
    expect(retrievedPlayerB).toEqual({ ...playerB, position: 1 });
  });

  it('cannot retrieve a missing player', async() => {
    await request(server)
      .get(`/v1/leaderboard/player/testplayer`)
      .expect(404);
  });

  it('updates player score', async() => {
    const player = { username: 'aaa', score: 0 };
    await createPlayerInDatabase(player);

    const newScore = 20;
    const { body: updatedPlayer } = await request(server)
      .patch(`/v1/leaderboard/player/${player.username}`)
      .set('Content-Type', 'application/json')
      .send({ score: newScore })
      .expect(200);

    expect(updatedPlayer).toEqual({ ...player, score: newScore });
    const savedPlayers = await getPlayersFromDatabase();
    expect(savedPlayers.length).toEqual(1);
    expect(savedPlayers[0].username).toEqual(player.username);
    expect(savedPlayers[0].score).toEqual(newScore);
  });

  it('cannot update a player without score', async() => {
    await request(server)
      .patch(`/v1/leaderboard/player/testplayer`)
      .set('Content-Type', 'application/json')
      .send({})
      .expect(400);
  });

  it('cannot update a missing player score', async() => {
    await request(server)
      .patch(`/v1/leaderboard/player/testplayer`)
      .set('Content-Type', 'application/json')
      .send({ score: 10 })
      .expect(404);
  });

  it('retrieves a leaderboard', async() => {
    const playerA = { username: 'aaa', score: 5 };
    const playerB = { username: 'bbb', score: 3 };
    const playerC = { username: 'ccc', score: 10 };
    const playerD = { username: 'ddd', score: 1 };
    await Promise.all([playerA, playerB, playerC, playerD]
      .map(player => createPlayerInDatabase(player)));

    const { body: leaderboard } = await request(server)
      .get(`/v1/leaderboard`)
      .expect(200);

    expect(leaderboard.players[0]).toEqual({ ...playerC, position: 1 });
    expect(leaderboard.players[1]).toEqual({ ...playerA, position: 2 });
    expect(leaderboard.players[2]).toEqual({ ...playerB, position: 3 });
    expect(leaderboard.players[3]).toEqual({ ...playerD, position: 4 });
  });

  it('clears a leaderboard', async() => {
    const playerA = { username: 'aaa', score: 0 };
    const playerB = { username: 'bbb', score: 10 };
    await createPlayerInDatabase(playerA);
    await createPlayerInDatabase(playerB);

    const { body: leaderboard } = await request(server)
      .delete(`/v1/leaderboard`)
      .expect(200);

    expect(leaderboard.players.length).toEqual(0);
    const savedPlayers = await getPlayersFromDatabase();
    expect(savedPlayers.length).toEqual(0);
  });

  function initDatabase(): Promise<void> {
    return new Promise((resolve, reject) => {
      const initSql = fs.readFileSync(`${__dirname}/../../player-db-init.sql`);
      database.exec(initSql.toString(), (err) => {
        if (err) {
          reject(err);
        }
        resolve();
      });
    });
  }

  async function createPlayerInDatabase(player: Player): Promise<void> {
    return new Promise((resolve, reject) => {
      const sql = `insert into player(username, score) values ('${player.username}', ${player.score})`;
      database.exec(sql, (err) => {
        if (err) {
          reject(err);
        }
        resolve();
      });
    });
  }

  function getPlayersFromDatabase(): Promise<Player[]> {
    return new Promise((resolve, reject) => {
      database.all('SELECT username, score FROM player', (err, rows: Player[]) => {
        if(err) {
          reject(err);
        }
        resolve(rows);
      });
    });
  }
});
