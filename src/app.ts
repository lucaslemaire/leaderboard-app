import Koa from 'koa';
import http, { Server } from 'http';
import LeaderboardService from './leaderboard/service';
import HttpRequestHandler from './leaderboard/http/request-handler';
import HttpRouter from './leaderboard/http/router';
import PlayerRepository from './leaderboard/persistence/player-repository';
import ConnectDatabase, { DbConfig } from './leaderboard/persistence';
import bodyParser from 'koa-bodyparser';

interface AppConfig {
  server: {
    port: string
  },
  db: DbConfig
}

interface App {
  start(config: AppConfig): Promise<Server>
  stop(): Promise<void>
}

export default function App(): App {
  let server: Server = null;

  async function start(config: AppConfig) {
    const { server: { port: listenPort }, db: dbConfig } = config;
    const database = await ConnectDatabase(dbConfig);
    const playerRepository = PlayerRepository(database);
    const leaderboardService= LeaderboardService(playerRepository);
    const httpHandler = HttpRequestHandler(leaderboardService);
    const router = HttpRouter(httpHandler);

    const koa = new Koa();
    koa.use(bodyParser());
    koa.use(router.routes());

    server = http.createServer(koa.callback());
    return new Promise<Server>((resolve) => {
      server.listen(listenPort, () => {
        console.info(`Http server listening on ${listenPort}.`);
        return resolve(server);
      });
    });
  }

  function stop() {
    if (server === null) {
      return;
    }
    return new Promise<void>((resolve, reject) => {
      server.close((err) => {
        if (err) {
          return reject(err);
        }
        return resolve();
      });
    });
  }

  return {
    start,
    stop
  };
}
