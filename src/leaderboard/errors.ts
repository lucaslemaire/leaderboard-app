export class PlayerMissingError extends Error {}

export class PlayerConflictError extends Error {}
