import {
  Leaderboard,
  LeaderboardPlayer,
  Player,
} from './models';

import {
  PlayerConflictError,
  PlayerMissingError
} from './errors';

interface PlayerProvider {
  getAllNames(): Promise<string[]>
  getAllSortedByScore(): Promise<Player[]>
}

export interface PlayerRepository extends PlayerProvider {
  upsert(player: Player): Promise<void>
  deleteAll(): Promise<void>
}

export interface LeaderboardService {
  createPlayer(username: string): Promise<Player>
  updatePlayerScore(username: string, newScore: number): Promise<Player>
  getPlayer(username: string): Promise<LeaderboardPlayer | null>
  getLeaderboard(): Promise<Leaderboard>
  clearLeaderboard(): Promise<Leaderboard>
}

export default function Service(
  playerRepository: PlayerRepository,
): LeaderboardService {
  async function createPlayer(username: string): Promise<Player> {
    await ensurePlayerDoesNotExist(username);

    const player: Player = {
      username,
      score: 0,
    };
    await playerRepository.upsert(player);
    return player;
  }

  async function playerExists(username: string) {
    const playerNames = await playerRepository.getAllNames();
    return playerNames.includes(username);
  }

  async function ensurePlayerExists(username: string) {
    if (!await playerExists(username)) {
      throw new PlayerMissingError(`The player username ${username} does not exist`);
    }
  }

  async function ensurePlayerDoesNotExist(username: string) {
    if (await playerExists(username)) {
      throw new PlayerConflictError(`The player username ${username} is already taken`);
    }
  }

  async function updatePlayerScore(username: string, newScore: number): Promise<Player> {
    await ensurePlayerExists(username);

    const player: Player = {
      username,
      score: newScore,
    };
    await playerRepository.upsert(player);

    return player;
  }

  async function getPlayer(username: string): Promise<LeaderboardPlayer | null> {
    const players = await playerRepository.getAllSortedByScore();
    const playerIndex = players.findIndex(player => player.username === username);
    if (playerIndex === -1) {
      return null;
    }

    const player = players[playerIndex];
    return toLeaderboardPlayer(player, playerIndex);
  }

  async function getLeaderboard(): Promise<Leaderboard> {
    const players = await playerRepository.getAllSortedByScore();
    const leaderboardPlayers = players.map(
      (player, index) => toLeaderboardPlayer(player, index));
    return { players: leaderboardPlayers };
  }

  async function clearLeaderboard(): Promise<Leaderboard> {
    await playerRepository.deleteAll();
    return { players: [] };
  }

  function toLeaderboardPlayer(player: Player, playerIndex: number) : LeaderboardPlayer {
    return {
      ...player,
      position: playerIndex + 1
    };
  }

  return {
    getLeaderboard,
    createPlayer,
    updatePlayerScore,
    getPlayer,
    clearLeaderboard,
  };
}
