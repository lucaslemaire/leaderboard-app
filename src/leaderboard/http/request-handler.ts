import { LeaderboardService } from '../service';
import { Context } from 'koa';
import { PlayerConflictError, PlayerMissingError } from '../errors';

export interface LeaderboardRequestHandler {
  createPlayer(ctx: Context): Promise<void>
  updatePlayerScore(ctx: Context): Promise<void>
  getPlayer(ctx: Context): Promise<void>
  getLeaderboard(ctx: Context): Promise<void>
  clearLeaderboard(ctx: Context): Promise<void>
}

export interface CreatePlayerRequest {
  username: string
}

export interface UpdatePlayerScoreRequest {
  score: number
}

export default function RequestHandler(leaderboardService: LeaderboardService): LeaderboardRequestHandler {
  async function createPlayer(ctx: Context) {
    const { username } = <CreatePlayerRequest>ctx.request.body;
    if (!username) {
      ctx.status = 400;
      ctx.body = 'username not provided';
      return;
    }

    try {
      const player =  await leaderboardService.createPlayer(username);
      ctx.status = 201;
      ctx.body = player;
    } catch (error) {
      if (error instanceof PlayerConflictError) {
        ctx.status = 409;
        ctx.body = error.message;
      } else {
        throw error;
      }
    }
  }

  async function clearLeaderboard(ctx: Context) {
    const leaderboard =  await leaderboardService.clearLeaderboard();
    ctx.status = 200;
    ctx.body = leaderboard;
  }

  async function getLeaderboard(ctx: Context) {
    const leaderboard = await leaderboardService.getLeaderboard();
    ctx.status = 200;
    ctx.body = leaderboard;
  }

  async function getPlayer(ctx: Context) {
    const { username } = ctx.params;

    const player = await leaderboardService.getPlayer(username);
    if (!player) {
      ctx.status = 404;
      return;
    }
    ctx.status = 200;
    ctx.body = player;
  }

  async function updatePlayerScore(ctx: Context) {
    const { username } = ctx.params;
    const { score } = <UpdatePlayerScoreRequest>ctx.request.body;
    if (typeof score !== 'number') {
      ctx.status = 400;
      ctx.body = 'a score number must be provided in the request body';
      return;
    }

    try {
      const player = await leaderboardService.updatePlayerScore(username, score);
      ctx.status = 200;
      ctx.body = player;
    } catch (err) {
      if (err instanceof PlayerMissingError) {
        ctx.status = 404;
        ctx.body = err.message;
      } else {
        throw err;
      }
    }
  }

  return {
    createPlayer,
    clearLeaderboard,
    getLeaderboard,
    getPlayer,
    updatePlayerScore,
  };
}
