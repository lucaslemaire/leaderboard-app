import Router from 'koa-router';
import { LeaderboardRequestHandler } from './request-handler';

export default function HttpRouter(handler: LeaderboardRequestHandler): Router {
  const router: Router = new Router();

  router.get('/v1/leaderboard', handler.getLeaderboard);
  router.get('/v1/leaderboard/player/:username', handler.getPlayer);
  router.post('/v1/leaderboard/player', handler.createPlayer);
  router.patch('/v1/leaderboard/player/:username', handler.updatePlayerScore);
  router.delete('/v1/leaderboard', handler.clearLeaderboard);

  return router;
}
