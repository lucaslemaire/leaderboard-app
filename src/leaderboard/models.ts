export type Player = {
  username: string
  score: number
}

export type LeaderboardPlayer = Player & {
  position: number
}

export type Leaderboard = {
  players: LeaderboardPlayer[]
}
