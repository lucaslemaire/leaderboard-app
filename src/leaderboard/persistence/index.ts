import { Database } from 'sqlite3';

export interface DbConfig {
  path: string
}

function ConnectDatabase (config: DbConfig): Promise<Database> {
  return new Promise((resolve, reject) => {
    const db = new Database(config.path, (err) => {
      if (err) {
        return reject(err.message);
      }
      return resolve(db);
    });
  });
}

export default ConnectDatabase;
