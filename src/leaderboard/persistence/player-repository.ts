import { PlayerRepository } from '../service';
import { Player } from '../models';
import { Database } from 'sqlite3';

export default function PlayerRepository(
  db: Database
): PlayerRepository {
  function query(sqlQuery: string, parameters: unknown[]){
    return new Promise((resolve, reject) => {
      db.all(sqlQuery, parameters, (err, rows) => {
        if (err) {
          return reject(err.message);
        }
        return resolve(rows);
      });
    });
  }

  async function upsert(player: Player) {
    const sqlQuery = 'INSERT OR REPLACE INTO player(username, score) VALUES(?, ?)';
    await query(sqlQuery, [player.username, player.score]);
  }

  async function getAllSortedByScore() {
    const sqlQuery = 'SELECT username, score FROM player ORDER BY score DESC';
    return <Player[]>await query(sqlQuery, []);
  }

  async function getAllNames() {
    const sqlQuery = 'SELECT username FROM player';
    const players = <Player[]>await query(sqlQuery, []);
    return players.map(player => player.username);
  }

  async function deleteAll() {
    const sqlQuery = 'DELETE FROM player';
    await query(sqlQuery, []);
  }

  return {
    upsert,
    getAllSortedByScore,
    getAllNames,
    deleteAll
  };
}
