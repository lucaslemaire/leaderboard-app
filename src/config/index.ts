export function env(name: string, defaultValue: string): string {
  return process.env[name] || defaultValue;
}

export default {
  server: {
    port: env('PORT', '3001'),
  },
  db: {
    path: env('LEADERBOARD_SQLITE_DB_PATH', `${__dirname}/../../player.db`)
  }
};
