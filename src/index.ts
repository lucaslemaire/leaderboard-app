import app from './app';
import config from "./config";

const { start } = app();

function handleError(error: unknown) {
  console.error(error);
  process.exit(1);
}

start(config).catch(handleError);

process.on('unhandledRejection', handleError);
