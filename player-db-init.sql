create table player
(
    id       integer primary key,
    username text    not null unique,
    score    integer not null
);