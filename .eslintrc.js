module.exports = {
  parser: '@typescript-eslint/parser',
  extends: [
    'plugin:@typescript-eslint/recommended'
  ],
  parserOptions: {
    project: './tsconfig.json',
    ecmaVersion: 2022
  },
  rules: {
    indent: ["error", 2],
    semi: "error",
    "object-curly-spacing": ["error", "always"]
  }
}
